package com.jzb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jzb.service.ProductServiceFeignDAO;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@RefreshScope
@RestController
public class OrderServiceApplication {
	@Autowired
	ProductServiceFeignDAO productServiceFeignDAO;
	
    @Value("${configParam}")
    private String configParam;

    @RequestMapping("/configParam")
    public String configParam() {

        return this.configParam;
    }

	public String getConfigParam() {
		return configParam;
	}

	public void setConfigParam(String configParam) {
		this.configParam = configParam;
	}


	public static void main(String[] args) {
		SpringApplication.run(OrderServiceApplication.class, args);
	}
	
 
    @RequestMapping("/")
    public String home() {
        return "Hello Order Service World";
    }
    
    @RequestMapping(value = "/hello" ,method = RequestMethod.GET)
    public ResponseEntity<String> hello() {
      System.out.println("hello order service1");
      return new ResponseEntity<>("hello order service1", HttpStatus.OK);
    }
    


    //订单信息需要调用产品信息  
	@RequestMapping(value = "/product/hello", method = RequestMethod.GET)
	public String getHelloFromProductService() {
	    return productServiceFeignDAO.hello();
	}
	
}
