package com.jzb.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient("product-service")
public interface ProductServiceFeignDAO {

   
    @RequestMapping(method = RequestMethod.GET, value = "/hello")
    String hello();

}